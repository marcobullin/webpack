# webpack

## Installation

```npm i```

## Projekt bauen ohne Webpack-Dev-Server

```npm run build```

## Projekt bauen mit Webpack-Dev-Server

```npm run dev:build```

### Projekt starten ohne Webpack-Dev-Server

Die Datei ```build/index.html``` öffnen

### Projekt starten mit Webpack-Dev-Server

Schauen was die Ausgabe vom Webpack-Dev-Server war (wahrscheinlich localhost:8081)





