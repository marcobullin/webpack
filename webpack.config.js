// const UglifyJsPlugin = require('uglifyjs-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const extractSASS = new ExtractTextPlugin('styles.css');

module.exports = {
  entry: './src/js/app.js',
  output: {
    path: `${__dirname}/build/public/`,
    filename: 'bundle.js',
    publicPath: 'public/'
  },
  plugins: [
    /*
    new UglifyJsPlugin({
      sourceMap: true
    }),
    */
    extractSASS
  ],
  devtool: 'source-map',
  module: {
    rules: [{
      test: /\.scss$/,
      use: extractSASS.extract(['css-loader', 'sass-loader'])
    }, {
      test: /\.js$/,
      exclude: /node_modules/,
      use: 'babel-loader'
    }]
  }
}
